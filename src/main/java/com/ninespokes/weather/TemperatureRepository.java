package com.ninespokes.weather;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface TemperatureRepository extends MongoRepository<Temperature, String>{
	public Temperature findByLocationOrderByTimestampDesc(String location);


}
