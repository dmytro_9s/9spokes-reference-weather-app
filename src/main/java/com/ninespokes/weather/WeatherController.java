package com.ninespokes.weather;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.ninespokes.weather.openweathermap.OpenWeatherMap;

@Controller
@RequestMapping("/temperature")
public class WeatherController {

	private static final int SIXTY_SECONDS = 60000;

	private static OpenWeatherMap openWeatherMap;
	private static final Logger log = LoggerFactory.getLogger(WeatherController.class);
	private static Temperature temperature = null;
	@Autowired
    private ApiConfiguration apiConfiguration;
	@Autowired
	private TemperatureRepository repository;

	@RequestMapping(method = RequestMethod.GET)
	public @ResponseBody Temperature temperature(
			@RequestParam(value = "location", defaultValue = "Auckland") String location) {
		Instant instant = Instant.now();
		long timeStampMillis = instant.toEpochMilli();
		Temperature temperatureFromMongo = repository.findByLocationOrderByTimestampDesc(location);

		if (temperatureFromMongo==null || (temperatureFromMongo != null
				&& Math.abs(temperatureFromMongo.getTimestamp() - timeStampMillis) > SIXTY_SECONDS)) {
			RestTemplate restTemplate = new RestTemplate();
			openWeatherMap = restTemplate.getForObject(
					apiConfiguration.buildRequest(location),
					OpenWeatherMap.class);
			log.info("Refreshed current temperature in " + location + ":" + openWeatherMap.getMain().getTemp());
			temperature = new Temperature(timeStampMillis, location, openWeatherMap.getMain().getTemp().toString());
			repository.save(temperature);
			log.info("Seved t for " + location + "to MongoDB");
		} else {
			log.info("Returning t for " + location + "from MongoDB");
			return temperatureFromMongo;
		}

		return temperature;
	}

	
	public static OpenWeatherMap getOpenWeatherMap() {
		return openWeatherMap;
	}

	public static void setOpenWeatherMap(OpenWeatherMap openWeatherMap) {
		WeatherController.openWeatherMap = openWeatherMap;
	}

}
