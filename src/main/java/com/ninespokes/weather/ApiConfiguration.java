package com.ninespokes.weather;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("classpath:com/ninespokes/weather/application.properties")
public class ApiConfiguration {
	@Value("${api.key}")
	private String apiKey;
	@Value("${api.root}")
	private String apiUrl;
	public String getApiKey() {
		return apiKey;
	}
	public void setApiKey(String apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiUrl() {
		return apiUrl;
	}
	public void setApiUrl(String apiUrl) {
		this.apiUrl = apiUrl;
	}
	
	public String buildRequest(String location) {
		return this.apiUrl + "?q=" + location + "&appid=" + apiKey + "&units=metric";
	}
   
}
