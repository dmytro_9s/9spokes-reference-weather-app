package com.ninespokes.weather;

import org.springframework.data.annotation.Id;

public class Temperature {
	
	public Temperature() {
		super();
	}

	@Id
	public long timestamp;
	public String location;
	public String temperature;

	public Temperature(long timestamp, String location, String temperature) {
		this.timestamp = timestamp;
		this.location = location;
		this.temperature = temperature;
	}

	public String getLocation() {
		return location;
	}

	public String getTemperature() {
		return temperature;
	}

	public long getTimestamp() {
		return timestamp;
	}

	@Override
	public String toString() {
		return String.format("Temperature[timestamp=%s, location='%s', temperature='%l']", timestamp, location,
				temperature);
	}
}
