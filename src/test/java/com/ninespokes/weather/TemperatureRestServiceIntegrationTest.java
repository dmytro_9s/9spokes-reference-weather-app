package com.ninespokes.weather;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Import(ApiConfiguration.class)
public class TemperatureRestServiceIntegrationTest {

	@Autowired
	private TestRestTemplate restTemplate;

	/*
	 * This test will require local docker container  with mongodb running with exposed port 27017 to be successful.
	 */
	@Test
	public void simpleMongoDbTest() {

		ResponseEntity<Temperature> responseEntity =

				restTemplate.getForEntity("/temperature", Temperature.class);

		Temperature temperature = responseEntity.getBody();
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
		assertEquals("Auckland", temperature.getLocation());

	}

}
